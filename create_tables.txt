CREATE TABLE usuario (
    cpf varchar(255) PRIMARY KEY,
    nome varchar(300) NOT NULL,
    sobrenome varchar(255) NOT NULL,
    email_principal varchar(255) UNIQUE,	 
    email_familiar varchar(255) NOT NULL,
    email_opcional varchar(255),
    contato_familiar varchar(255) NOT NULL,
    contato_opcional varchar(255),
    media_sono varchar(255)
);


CREATE TABLE dados (
    id SERIAL PRIMARY KEY ,
    data_hora timestamp,
    quantidade_sono varchar(255),
    cpf_usuario varchar(255) NOT NULL,
	FOREIGN KEY(cpf_usuario) REFERENCES usuario(cpf)
);


CREATE TABLE login (
    id SERIAL,
    access_token varchar(300),
    refresh_token varchar(255),
    cpf_usuario varchar(255) NOT NULL,
	FOREIGN KEY(cpf_usuario) REFERENCES usuario(cpf)
);
