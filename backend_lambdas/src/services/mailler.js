"use strict";
const nodemailer = require('nodemailer')

module.exports = class Mailler {
    transporter
    constructor() {
        this.transporter = nodemailer.createTransport({
            host: "smtp-relay.sendinblue.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
            user: process.env.USER_SMTP, // generated ethereal user
            pass: process.env.PASS_SMTP, // generated ethereal password
            },
        })
    }

    async sendMessageLogin (users) {
        try{           
            for (const user of users ) {
                await this.transporter.sendMail({
                    from: '"Marcel da Sleep 👻" <marcellorenzo@gmail.com>', // sender address
                    to: [user.email, user.email_familiar], // list of receivers
                    subject: "Sistema Sleep Analytics", // Subject line
                    text: "Olá!", // plain text body
                    html:'Olá '+user.nome+'! Precisamos que faça login no nosso sistema para continuarmos analisando os dados de sono :) </b> <br> <a> localhost:3000 </a>'
                })

                console.log('Email enviado com sucesso.')

                console.groupCollapsed('Mensagem:')
                console.log('Olá '+user.nome+'! Precisamos que faça login no nosso sistema para continuarmos analisando os dados de sono :) ')
                console.groupEnd()
            }

            return 'success'
                    
        }catch(err) {
            throw err
        }
    }


    async sendAlert (users) {
        try{
            for (const user of users ) {
                let data = []
                if (user.lastFiveSleepTimes.length > 0) {
                    user.lastFiveSleepTimes.forEach((element) => {
                        const sono = element.Sono || ''
                        const date = element.Data || ''
                        data = data.concat(`<p> Sono: ${sono}h Data: ${date.toLocaleString('pt-BR')} </p>`)
                    })
                }               

                await this.transporter.sendMail({
                    from: '"Marcel da Sleep 👻" <marcellorenzo@gmail.com>', // sender address
                    to: [user.email, user.email_familiar], // list of receivers
                    subject: "Sistema Sleep Analytics", // Subject line
                    text: "Olá!", // plain text body
                    html: 'ALERTA! Os dados de Sono devem ser monitorados. Há avariações. </b> <br> <p> Ultimos 5 tempos de sono: '+data+'</p>'
                })

                console.log('Email enviado com sucesso.')

                console.groupCollapsed('Mensagem:')
                console.log('ALERTA! Os dados de Sono devem ser monitorados. Há avariações. Ultimos 5 tempos de sono: ')
                console.log(data)
                console.groupEnd()
            }

            return 'success'
                    
        }catch(err) {
            console.log(err)
            throw err
        }
    }
}