const HttpClient = require('../infra/http/axios')
const http = new HttpClient()

module.exports = class Sleep {
    async get(access_token) {
        try{
            const params = {
                startTime: '2020-03-14T00:00:00.000Z',
                endTime: '2020-03-14T13:10:00.000Z',
                activityType: '72'
            }

            const headers = {
                'Authorization': `Bearer ${access_token}`
            }

            const response = await http.request({ url: process.env.DATA_URL, method: 'get', headers, params })

            console.log('Google API Response', { response: response.data })
            console.log('Google API Sessions', { Sessions: response.data.session })

            const { data: { session } } = response

            if (session.length > 0) {
                const { activeTimeMillis } = session[0]

                const timeSleep = (activeTimeMillis/3600000).toFixed(2)

                return timeSleep  
            }

            return 0
            
        }catch(err) {
            throw err
        }
    }
}