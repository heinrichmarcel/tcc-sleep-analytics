module.exports = class GetDados {
    constructor(sgbd) {
        this.sgbd = sgbd
        this.format = require('pg-format')
    }

    async get(){
        try{
            const { userRepository, loginRepository, dadosRepository } = require('../repositories')
            const { refreshService, sleepService, checkDataService } = require('./')

            const rows = await userRepository.select()

            console.log('Pegando dados do Usuário.')

            let userToLogin = []
            let userAlert = []

            for (const row of rows) {               
                if(row.cpf !== '03323634063') continue

                console.log({ row })

                const loginData = await loginRepository.selectByCpf(row.cpf)

                if (loginData.length < 1) {
                    userToLogin.push({ email: row.email_principal, email_familiar: row.email_familiar, nome: row.nome })
                    continue
                } 
                
                const { refresh_token } = loginData[0]

                console.log('Pegando refresh_token do usuário', { refresh_token })


                if (!refresh_token) {
                    userToLogin.push({ email: row.email_principal, email_familiar: row.email_familiar, nome: row.nome })
                    continue
                } 


                const access_token = await refreshService.refresh(refresh_token)

                console.log('Pegando access_token a partir do refresh token', { access_token })

                if (access_token.error) {
                    userToLogin.push({ email: row.email_principal, email_familiar: row.email_familiar, nome: row.nome })
                    continue
                }

                console.log('Realizando update do access_token no banco...')

                await loginRepository.update(access_token, row.cpf)

                const sleepTime = await sleepService.get(access_token)

                console.log('Pegando dados de sono na API Google Fit', { sleepTime })

                console.log('Realizando insert do valor dos dados de sono no banco ...')

                const check = await checkDataService.check({ usuario: row.cpf, email: row.email_principal, email_familiar: row.email_familiar, sleepTime })

                console.log('Realizando checagem dos dados de sono', { irritability: check })

                if (check){
                    const lastFive = await dadosRepository.selectLastFive(row.cpf)

                    const lastFiveSleepTimes = lastFive.map(el => ({ Sono: el.quantidade_sono, Data: el.data_hora }))

                    console.log('Pegando ultimos 5 dados de sono da tabela para envio de alerta.')

                    userAlert.push({ usuario: row.cpf, email: row.email_principal, email_familiar: row.email_familiar, nome: row.nome, sleepTime, lastFiveSleepTimes })
                } 

                continue
            }

            return { userToLogin, userAlert }

        }catch(err){
            throw err
        }
    }
}