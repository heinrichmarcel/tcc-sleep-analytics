module.exports = class CheckData {

    async check(users) {
        try{
            const { dadosRepository, userRepository } = require ('../repositories')

            const { usuario, sleepTime } = users

            const { quantidade_sono } = await dadosRepository.selectByUser(usuario)

            const sleepTimeDiff = Number(quantidade_sono) - Number(sleepTime)

            let { media_sono } = await userRepository.selectByCpf(usuario)

            if (!media_sono) media_sono = 0

            const newMedia = (Number(media_sono) + Number(sleepTime))/2

            await userRepository.updateMedia(newMedia, usuario)

            const irritability = Math.abs(sleepTimeDiff) > 1 || sleepTime < 7 || newMedia < 7 
            
            await dadosRepository.insert({ usuario, sleepTime, notification: irritability })

            return irritability
        }catch(err){
            throw err
        }
    }
}