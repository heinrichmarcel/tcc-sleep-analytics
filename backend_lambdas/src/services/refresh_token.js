const HttpClient = require('../infra/http/axios')
const http = new HttpClient()


module.exports = class RefreshToken {  
    async refresh(refresh_token) {
        try{
            const params = {
                client_id: process.env.CLIENT_ID,
                client_secret: process.env.CLIENT_SECRET,
                grant_type: 'refresh_token',
                refresh_token
            }

            const response = await http.request({ url: process.env.REFRESH_TOKEN_URL, method: 'post', data : { ...params }})

            if (response.data.error) return { error: response.data.error_description } 

            return response.data.access_token

        } catch (error){
            console.log(error)
            throw new Error(error)
        }


    }
} 