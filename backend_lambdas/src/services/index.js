const Mailler = require('./mailler')
const maillerService = new Mailler()

const CheckData = require('./check_data')
const checkDataService = new CheckData()

const RefreshToken = require('./refresh_token')
const refreshService = new RefreshToken()

const Sleep = require('./sleep')
const sleepService = new Sleep()

const GetDados = require('./get_dados')
const dadosService = new GetDados()


module.exports = {
    maillerService,
    checkDataService,
    refreshService,
    sleepService,
    dadosService
}