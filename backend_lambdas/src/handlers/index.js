"use strict";
const { db } = require('../repositories')
const { maillerService, checkDataService, dadosService } = require('../services')

module.exports.handler = async (event) => {
  await db.getConnection()

  console.log('Cron acionada. Handler sendo executado.')

  const dados = await dadosService.get()

  let successLogin, successAlert

  // if (dados.userToLogin.length > 0) successLogin = await maillerService.sendMessageLogin(dados.userToLogin, 'login')
  if (dados.userAlert.length > 0 ) successAlert = await maillerService.sendAlert(dados.userAlert, 'alert')

  return { successLogin, successAlert }
};
