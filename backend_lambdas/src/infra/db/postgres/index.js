const { Client } = require('pg')
const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'sleep',
    password: 'postgres',
    port: 5432,
})

module.exports = class Pg {

    async getConnection() {  
        await client.connect()
    }

    async endConnection(){
        await client.end()
    }

    async query(content){
        try{

            const res = await client.query(content)
        
            return res
        }catch(err){
            throw new Error(err)
        }    
    }
}

