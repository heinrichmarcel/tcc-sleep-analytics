module.exports = class Login {
    constructor(sgbd) {
        this.sgbd = sgbd
        this.format = require('pg-format')
    }


    async selectByCpf(cpf) {
        try{
            const sql = this.format('SELECT refresh_token FROM login WHERE cpf_usuario = %L', cpf)

            const { rows } = await this.sgbd.query(sql)

            return rows
        }catch(err){
            throw new Error(err)
        }
    }

    async update(access_token, cpf){
        try{
            const sqlUpdate = this.format('UPDATE login SET access_token = %L WHERE cpf_usuario = %L', access_token, cpf)

            await this.sgbd.query(sqlUpdate)
        }catch(err){
            throw err
        }
    }
}