const DB = require('../infra/db/postgres')
const db = new DB()

const User = require('./user')
const userRepository = new User(db)

const Login = require('./login')
const loginRepository = new Login(db)

const Dados = require('./dados')
const dadosRepository = new Dados(db)

module.exports = {
    userRepository,
    loginRepository,
    dadosRepository,
    db
}