module.exports = class Dados {
    constructor(sgbd) {
        this.sgbd = sgbd
        this.format = require('pg-format')
    }

    async selectByUser(cpf) {
        try{
            const sql = this.format('SELECT quantidade_sono FROM dados WHERE cpf_usuario = %L', cpf)

            const { rows } = await this.sgbd.query(sql)

            return rows[0]
        }catch (err) {
            throw err
        }
    }

    async selectLastFive(cpf) {
        const sql = this.format('SELECT quantidade_sono, data_hora FROM dados WHERE cpf_usuario = %L ORDER BY id desc LIMIT 5', cpf)

        const { rows } = await this.sgbd.query(sql)

        return rows
    }

    async insert(params) {
        try{
            const now = new Date()

            const values = [ now, params.sleepTime, params.usuario, params.notification ]

            const sql = this.format('INSERT INTO dados (data_hora, quantidade_sono, cpf_usuario, notification) VALUES (%L)', values)

            await this.sgbd.query(sql)
        }catch(err){
            throw new Error(err)
        }
    }
}