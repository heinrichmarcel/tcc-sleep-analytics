module.exports = class User {
    constructor(sgbd) {
        this.sgbd = sgbd
        this.format = require('pg-format')
    }

    async select() {
        try{
            const select = this.format('SELECT cpf, email_principal, email_familiar, nome, media_sono FROM usuario')

            const { rows } = await this.sgbd.query(select)

            return rows
        }catch(err){
            throw new Error(err)
        }
    }

    async selectByCpf(cpf) {
        try{
            const select = this.format('SELECT cpf, email_principal, email_familiar, nome, media_sono FROM usuario WHERE cpf = %L', cpf)

            const { rows } = await this.sgbd.query(select)

            return rows
        }catch(err){
            throw new Error(err)
        }
    }

    async updateMedia(time, cpf) {
        try{

            const sqlUpdate = this.format('UPDATE usuario SET media_sono = %L WHERE cpf = %L', time, cpf)

            await this.sgbd.query(sqlUpdate)
        }catch(err){
            throw new Error(err)
        }
    }
}