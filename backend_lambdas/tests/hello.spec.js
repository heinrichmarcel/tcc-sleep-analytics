'use strict';

const sut = require('../src/handlers/handler')

describe('hello', () => {
  it('implement tests here', async () => {
    const event = {}
    const actual = await sut.hello(event)

    expect(actual.body).toStrictEqual('Go Serverless v2.0! Your function executed successfully!')
  });
});
