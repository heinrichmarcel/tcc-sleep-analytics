const config = require('../config')


module.exports = class Sleep {
    constructor(client) {
        this.client = client
    }

    async get(access_token) {
        try{
            const params = {
                startTime: '2019-12-10T00:00:00.000Z',
                endTime: '2019-12-11T10:10:00.000Z',
                activityType: '72'
            }

            const headers = {
                'Authorization': `Bearer ${access_token}`
            }

            const {data : { session }} = await this.client.request({ url: config.data.url, method: 'get', headers, params})

            console.log(session)

            if(session.length > 0) {
                const { activeTimeMillis } = session[0]

                const timeSleep = (activeTimeMillis/3600000).toFixed(2)

                return timeSleep  
            }

            return 0
            
        }catch(err) {
            throw err
        }
    }
}