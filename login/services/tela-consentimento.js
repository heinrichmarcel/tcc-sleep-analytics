const HttpClient = require('../infra/http/axios')
const http = new HttpClient()
const config = require('../config')

module.exports = class Consent {

    async getConsent() {
        try{
            const response = await http.request({
                url: config.consent.url,
                method: 'get', 
                params: {
                    client_id: config.client.id,
                    redirect_uri: config.client.redirectUri,
                    scope: config.consent.scope,
                    response_type: 'code'
                }
            })

            return response.data
        }catch(err){
            throw new Error(err)
        }
    }
}