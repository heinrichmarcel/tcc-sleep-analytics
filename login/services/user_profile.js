const config = require('../config')
const HttpClient = require('../infra/http/axios')
const http = new HttpClient()

module.exports = class UserProfile {
    async get(access_token) {
        try{
            const headers = {
                Authorization: `Bearer ${ access_token }`
            }

            const { data: { emailAddresses }} = await http.request({ url: String(config.user.profileUrl), method: 'get', headers })
            
            const email = emailAddresses.map((element) => element.value)

            return email[0]
        }catch(err){
            console.log(err)
            throw err
        }
    }
}