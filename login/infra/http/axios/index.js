const axios = require('axios')

module.exports = class Axios {

    async request(params) {
        try{
            const response = await axios.request({...params})

            return response
        }catch(err){
            throw new Error(err)
        }
    }
}