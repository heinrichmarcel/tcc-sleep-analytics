module.exports = class Login {
    constructor(sgbd) {
        this.sgbd = sgbd
        this.format = require('pg-format')
    }

    async insert(params) {
        const config =  require('../config')
        try{
            const select = this.format('SELECT * FROM login WHERE cpf_usuario = %L', params.cpf)

            const { rows } = await this.sgbd.query(select)

            const columns = config.login.attributes

            const values = [ params.access_token, params.refresh_token, params.cpf ]

            let query, sql

            if (rows.length === 0 ) {
                sql = this.format('INSERT INTO login ('+columns+') VALUES (%L)', values)

                query = await  this.sgbd.query(sql)

                return query
            }
            
            sql = this.format('UPDATE login SET access_token = %L, refresh_token = %L WHERE cpf_usuario = %L', params.access_token, params.refresh_token, params.cpf)

            query = await  this.sgbd.query(sql)

            return query
        }catch(err){
            throw new Error(err)
        }
    }
}