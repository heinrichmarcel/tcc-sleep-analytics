module.exports = class User {
    constructor(sgbd) {
        this.sgbd = sgbd
        this.format = require('pg-format')
    }

    async create(user) {
        try{
            const account = await this.sgbd.query(`SELECT * from user where email == ${user.email}`)
            if(!account) await this.sgbd.query(`INSERT INTO user VALUES() `)
        }catch(err){
            throw err
        }
    }

    async selectByEmail(email) {
        try{
            const sql = this.format('SELECT cpf FROM usuario WHERE email_principal = %L', email)

            const { rows } = await this.sgbd.query(sql)

            if (rows.length < 1 ) throw new Error('Este email não possui cadastro!')

            const cpf = rows[0].cpf

            return cpf
        }catch(err){
            throw err
        }
    }

    async selectLastFive(cpf) {
        const sql = this.format('SELECT quantidade_sono, data_hora, notification FROM dados WHERE cpf_usuario = %L ORDER BY id desc LIMIT 5', cpf)

        const { rows } = await this.sgbd.query(sql)

        return rows
    }

    async insert(params) {
        const config =  require('../config')
        try{
            const select = this.format('SELECT * FROM usuario WHERE cpf = %L OR email_principal = %L', params.doc, params.principal_email)

            const { rows } = await this.sgbd.query(select)

            if (rows.length > 0 ) throw new Error('User already exists.')
            
            const columns = config.user.attributes
            
            const values = [ params.doc, params.first_name, params.second_name, params.first_email, params.second_email, params.responsable_contact, params.optional_contact, params.principal_email]

            const sql = this.format('INSERT INTO usuario ('+columns+') VALUES (%L)', values)

            const query = await  this.sgbd.query(sql)

            return query
        }catch(err){
            throw err
        }
    }

    async insertData(params) {
        try{
            const now = new Date()

            const values = [ now, params.sleepTime, params.usuario, params.notification ]

            const sql = this.format('INSERT INTO dados (data_hora, quantidade_sono, cpf_usuario, notification) VALUES (%L)', values)

            await this.sgbd.query(sql)
        }catch(err){
            throw new Error(err)
        }
    }

    async selectData(params) {
        try{
            const select = this.format('SELECT data_hora FROM dados WHERE cpf_usuario = %L ', params.doc)

            const { rows } = await this.sgbd.query(select)

            return rows
        }catch(err){
            throw new Error(err)
        }
    }
}