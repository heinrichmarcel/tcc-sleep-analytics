const express = require('express');
const app = express();
const session = require('express-session');
const bodyParser = require('body-parser')
const config = require('./config')
require('dotenv').config()

app.set('view engine', 'ejs');


app.use(session({
  resave: false,
  saveUninitialized: true,
  secret: 'SECRET' 
}));

app.get('/', async (req, res) => {
  await postgres.getConnection()
  res.render('pages/auth');
});

const port = process.env.PORT || 3000;
app.listen(port , () => console.log('App listening on port ' + port));

app.use(bodyParser.urlencoded({ extended: true }))

app.set('view engine', 'ejs');

app.get('/error', (req, res) => res.send("error logging in"));


const Pg = require('./infra/db/postgres')
const postgres = new Pg()
 

app.get('/login', async (req, res) => {
  res.redirect(`${ config.consent.url }`)
})

app.get('/create', async (req, res) => {
  res.render('pages/cadastro');
})

app.get('/home', async (req, res) => {
  const Pg = require('./infra/db/postgres')
  const postgres = new Pg()

  const User = require('./repositories/user')
  const bd_access = new User(postgres)

  const response = await bd_access.selectLastFive('03323634063')

  let table = []

  response.forEach((element) => {
    table.push({ 
      data_hora: element.data_hora.toLocaleString('pt-BR'),
      quantidade_sono: element.quantidade_sono,
      notification: element.notification
    })
  })

  res.render('pages/home', { cpf: req.body.cpf, table });
})

app.post('/cadastro', async (req, res) => {
  try{
    const User = require('./repositories/user')
    const bd_access = new User(postgres)
    
    const user_data = req.body

    await bd_access.insert(user_data)

    res.render('pages/auth-after-cadastro', { user_data })

  }catch (error) {
    res.render('pages/error', { error })
  }
})

app.get('/cadastro-sono',  async (req, res) => {
  try{
    res.render('pages/inserir-medicao')

  }catch (error) {
    res.render('pages/error', { error })
  }
})

app.post('/cadastro-sono',  async (req, res) => {
  try{
    const { quant_sono } = req.body

    const User = require('./repositories/user')
    const bd_access = new User(postgres)

    const dates = await bd_access.selectData({ doc: '03323634063' })

    const currentDate = new Date().toISOString().split(/T/)[0]

    for(const date of dates) {
      const dateFormated = date.data_hora.toISOString().split(/T/)[0]

      if (dateFormated === currentDate) throw new Error('Já existem dados para este dia.')
    }

    await bd_access.insertData({ sleepTime: quant_sono, usuario: '03323634063', notification: quant_sono > 9 || quant_sono < 11 })

    res.render('pages/inserir-medicao')

  }catch (error) {
    res.render('pages/error', { error })
  }
})

app.get('/cadastro-sono-success',  async (req, res) => {
  try{
    res.render('pages/success-sono')

  }catch (error) {
    res.render('pages/error', { error })
  }
})


app.get('/consent', async (req, res) => {
  try{
    const code = res.req.query.code

    const Token = require('./services/token')
    const token = new Token()
    
    const { access_token, refresh_token } = await token.get(code)

    console.log({ access_token }, { refresh_token })

    const UserProfile = require('./services/user_profile')
    const userProfile = new UserProfile()

    const emailUser = await userProfile.get(access_token)

    console.log(emailUser)

    const User = require('./repositories/user')
    const userRepository = new User(postgres)

    const cpf = await userRepository.selectByEmail(emailUser)

    if (!cpf) throw new Error('Email não cadastrado!')

    console.log(cpf)

    const Login = require('./repositories/login')
    const loginRepository = new Login(postgres)

    await loginRepository.insert({ cpf, access_token, refresh_token })

    res.render('pages/home' , { cpf })

  }catch (error) {
    res.render('pages/error', { error })
  }
})
