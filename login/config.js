require('dotenv').config()

const {
    CONSENT_URL,
    CLIENT_ID,
    CLIENT_SECRET,
    REDIRECT_URI,
    TOKEN_URL,
    DATA_URL,
    USER_ATTRIBUTES,
    USER_PROFILE_URL,
    LOGIN_ATTRIBUTES
} = process.env


const config = {
    consent: {
        url: CONSENT_URL
    },
    client: {
        id: CLIENT_ID,
        redirectUri: REDIRECT_URI,
        secret: CLIENT_SECRET
    },
    token: {
        url: TOKEN_URL
    },
    data: {
        url: DATA_URL
    },
    user: {
        attributes: USER_ATTRIBUTES,
        profileUrl: USER_PROFILE_URL
    },
    login: {
        attributes: LOGIN_ATTRIBUTES
    }
}

module.exports = config